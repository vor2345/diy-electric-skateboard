void ps2(void) 
{ 
  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed
  if (ps2x.ButtonPressed(PSB_START)) //will be TRUE as long as button is pressed
  {
    Serial.println("Start is being held");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
  }
  if (ps2x.ButtonPressed(PSB_SELECT))
  {
    Serial.println("Reset");
    digitalWrite(buzzer, LOW);
    digitalWrite(ledPin, LOW);
    ESC.writeMicroseconds(1000);
  }
  //    distance();
  if (ps2x.ButtonPressed(PSB_L3))
  {
    digitalWrite(ledPin, HIGH);
    Serial.println(" turn on");
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_R3))
  {
    digitalWrite(ledPin, LOW);
    Serial.println(" turn off");
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_L2))
  {
    int times = 1;
    Serial.println(" buzzer on");
    digitalWrite(buzzer, HIGH);
    delay(1500);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_R2))
  {
    int times = 1;
    Serial.println(" buzzer off");
    for (int i = 0; i <= times; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(100);
      digitalWrite(buzzer, LOW);
      delay(400);
    }
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_CIRCLE)) //will be TRUE if button was JUST pressed
  {
    int i = ESC.read();
    if (i <= 110)
      for (i; i <= 110; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 110; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1350);
    Serial.println(" speed 1");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_CROSS))//will be TRUE if button was JUST pressed OR released
  {
    int i = ESC.read();
    if (i <= 130)
      for (i; i <= 130; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 130; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1550);
    Serial.println(" speed 2");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_SQUARE))             //will be TRUE if button was JUST released
  {
    int i = ESC.read();
    if (i <= 140)
      for (i; i <= 140; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 140; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1650);
    Serial.println(" speed 3");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_TRIANGLE))
  {
    int i = ESC.read();
    if (i <= 150)
      for (i; i <= 150; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 150; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1850);
    Serial.println(" speed 4");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_L1) || ps2x.ButtonPressed(PSB_R1))
  { //print stick values if either is TRUE
    Serial.println(" stop");
    ESC.writeMicroseconds(1000);
    digitalWrite(buzzer, HIGH);
    delay(1000);
    digitalWrite(buzzer, LOW);
    digitalWrite(ledPin, LOW);
  }
}
