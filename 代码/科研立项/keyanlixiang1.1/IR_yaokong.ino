void IR(void)
{
  if (irrecv.decode(&results))         //开始接受IR数据
  {
    if (results.value == 0xFF38C7)//开始
    {
      Serial.println(" IR模式");
      Serial.println(results.value, HEX);//以16进制换行输出接收代码
      digitalWrite(buzzer, HIGH);
      delay(200);
      digitalWrite(buzzer, LOW);
      while (1)
      {
        //        distance();                   //安全距离判断
        if (irrecv.decode(&results))  //开始接受IR数据
        {
          if (results.value == 0xFF906F)//9号重启
          {
            ESC.writeMicroseconds(1000);
            digitalWrite(buzzer, HIGH);
            delay(200);
            digitalWrite(buzzer, LOW);
            digitalWrite(ledPin, LOW);
            Serial.println(" Reset");
            break;
          }
          if (results.value == 0xFF9867)//暂停
          {
            throttle = "stop";
            Action(throttle);
          }
          if (results.value == 0xFF10EF)//减少
          {
            throttle = "decrease";
            Action(throttle);
          }
          if (results.value == 0xFF5AA5)//增加
          {
            throttle = "increase";
            Action(throttle);
          }
          if (results.value == 0xFFA25D)//速度一
          {
            throttle = "one";
            Action(throttle);            //发送调用函数自变量
          }
          if (results.value == 0xFF629D)//速度二
          {
            throttle = "two";
            Action(throttle);          //发送调用函数自变量
          }
          if (results.value == 0xFFE21D)//速度三
          {
            throttle = "three";
            Action(throttle);           //发送调用函数自变量
          }
          if (results.value == 0xFF22DD)//速度四
          {
            throttle = "four";
            Action(throttle);          //发送调用函数自变量
          }
          if (results.value == 0xFF6897)//开灯
          {
            throttle = "turn on";
            Action(throttle);           //发送调用函数自变量
          }
          if (results.value == 0xFFB04F)//关灯
          {
            throttle = "turn off";
            Action(throttle);
          }
          if (results.value == 0xFF18E7)//铃铛尖悦响
          {
            throttle = "buzzer off";
            Action(throttle);
          }
          if (results.value == 0xFF4AB5)//铃铛响
          {
            throttle = "buzzer on";
            Action(throttle);
          }
          irrecv.resume(); // 接收下一个值
        }
        else continue;
        delay(10);
      }
    }
    delay(10);
  }
}
