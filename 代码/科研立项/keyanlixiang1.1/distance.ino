int distance()
{
  digitalWrite(TrigPin, HIGH);
  delayMicroseconds(50);
  digitalWrite(TrigPin, LOW);
  Time_Echo_us = pulseIn(EchoPin, HIGH);
  if ((Time_Echo_us < 60000) && (Time_Echo_us > 1))
  {
    Len_mm_X100 = (Time_Echo_us * 34) / 2;
    Len_Integer = Len_mm_X100 / 100;
    if (Len_Integer < 650)
    {
      ESC.writeMicroseconds(1000);
      digitalWrite(buzzer, HIGH);
      delay(100);
      digitalWrite(buzzer, LOW);
      Serial.print("Present Length is: ");
      Serial.println(Len_Integer);
      delay(1000);
    }
  }
}
/*超声波二次判断*/
//int distance()
//{
//  digitalWrite(TrigPin, HIGH);
//  delayMicroseconds(50);
//  digitalWrite(TrigPin, LOW);
//  Time_Echo_us = pulseIn(EchoPin, HIGH);
//  if ((Time_Echo_us < Time_Echo_us_max) && (Time_Echo_us > Time_Echo_us_min))
//  {
//
//    Len_mm_X100 = (Time_Echo_us * 34) / 2;
//    Len_Integer = Len_mm_X100 / 100;
//    if (Len_Integer < Len_Integer_level)                     //第一次判断
//    {
//      digitalWrite(TrigPin, HIGH);
//      delayMicroseconds(50);
//      digitalWrite(TrigPin, LOW);
//      Time_Echo_us = pulseIn(EchoPin, HIGH);
//      if ((Time_Echo_us < Time_Echo_us_max) && (Time_Echo_us > Time_Echo_us_min))
//      {
//
//        Len_mm_X100 = (Time_Echo_us * 34) / 2;
//        Len_Integer = Len_mm_X100 / 100;
//        if (Len_Integer < Len_Integer_level)                       //第二次判断
//        {
//          ESC.writeMicroseconds(speed_min);
//          digitalWrite(buzzer, HIGH);
//          delay(100);
//          digitalWrite(buzzer, LOW);
//          Serial.print("Present Length is: ");
//          Serial.println(Len_Integer);
//          delay(1000);
//        }
//      }
//    }
//  }
//}
