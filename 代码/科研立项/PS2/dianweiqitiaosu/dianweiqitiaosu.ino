#include <Servo.h> // Using servo library to control ESC
Servo esc; // Creating a servo class with name as esc
int val; // Creating a variable val
void setup()
{
esc.attach(9); // Specify the esc signal pin,Here as D9
esc.writeMicroseconds(1000); // initialize the signal to 1000
Serial.begin(9600);
}
void loop()
{
val= analogRead(A0); // Read input from analog pin a0 and store in val
val= map(val, 0, 1023,1000,2000); // mapping val to minimum and maximum(Change if needed)
Serial.println("val=");
Serial.println(val);
delay(200);
Serial.println("it=");
int it=analogRead(9);
Serial.println(it);
esc.writeMicroseconds(val); // using val as the signal to esc
}
