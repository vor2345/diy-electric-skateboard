void lanya(void)
{
  if (Serial.available() > 0)                   //手机蓝牙遥控
  {
    throttle  = Serial.readString();            //单片机接收数据赋值给字符串“throttle”
    Serial.println(" 手机蓝牙模式");
    if (throttle == "start")
    {
      Serial.println(" Start,  ready go!");     //开始启动程序
      digitalWrite(buzzer, HIGH);
      delay(300);
      digitalWrite(buzzer, LOW);
      while (1)
      {
        throttle  = Serial.readString();
        if (throttle.equals(""))
        {
          heart = 1;
          mTime = millis();
        }
        distance();
        Action(throttle);                     //发送调用函数自变量
        if (throttle == "reset")              //重新启动
        {
          Serial.println(" Reset");
          digitalWrite(buzzer, LOW);
          digitalWrite(ledPin, LOW);
          ESC.writeMicroseconds(1000);
          break;
        }
        if (heart == 1)                      //判断是否蓝牙断开，进而自保护
        {
          int test = millis();
          if (test - mTime > 6000)
          {
            ESC.writeMicroseconds(1000);
            digitalWrite(buzzer, LOW);
            digitalWrite(ledPin, LOW);
            Serial.println(" Reset");
            heart = 0;
          }
        }
      }
    }
    delay(2000);
    Serial.println(" Waiting...");
  }
}
