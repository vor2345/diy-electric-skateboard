void Action(String throttle)//主调函数
{
  if (throttle == "stop")                  //暂停，速度减为零
  {
    Serial.println(" Stop");
    ESC.writeMicroseconds(speed_level);
    digitalWrite(buzzer, LOW);
    digitalWrite(ledPin, LOW);
    throttle = "0";
  }
  else if (throttle == "decrease")              //调节滑板速度减少
  {
    int i = ESC.readMicroseconds();
    Serial.print(i);
    Serial.print("--");
    int t = i;
    if (i > speed_level)
      for (i; i >= t - speed_add; i--)
      {
        ESC.writeMicroseconds(i);
        delay(2);
      }
    digitalWrite(buzzer, HIGH);
    delay(100);
    digitalWrite(buzzer, LOW);
    Serial.println(i);
  }
  else if (throttle == "increase")              //调节滑板速度增加
  {
    int i = ESC.readMicroseconds();
    Serial.print(i);
    Serial.print("++");
    int t = i;
    if (i < speed_max ) //1900
      for (i; i <= t + speed_add; i++)
      {
        ESC.writeMicroseconds(i);
        delay(2);
      }
    digitalWrite(buzzer, HIGH);
    delay(400);
    digitalWrite(buzzer, LOW);
    Serial.println(i);
  }
  else if (throttle == "one")              //调节滑板速度为1
  {
    int i = ESC.readMicroseconds();
    Serial.print(i);
    Serial.print("--");
    if (i <= speed_one)
      for (i; i <= speed_one; i++)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    else
      for (i; i >= speed_one; i--)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    Serial.println(i);
    Serial.println(" speed 1");
    for (int i = 0; i <= 0; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(300);
      digitalWrite(buzzer, LOW);
    }
    throttle = "0";
  }
  else if (throttle == "two")              //调节滑板速度为2
  {
    int i = ESC.readMicroseconds();
    Serial.print(i);
    Serial.print("--");
    if (i <= speed_two)
      for (i; i <= speed_two; i++)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    else
      for (i; i >= speed_two; i--)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    Serial.println(i);
    Serial.println(" speed 2");
    for (int i = 0; i <= 1; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(300);
      digitalWrite(buzzer, LOW);
      delay(200);
    }
    throttle = "0";
  }
  else if (throttle == "three")              //调节滑板速度为3
  {
    int i = ESC.readMicroseconds();
    Serial.print(i);
    Serial.print("--");
    if (i <= speed_three)
      for (i; i <= speed_three; i++)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    else
      for (i; i >= speed_three; i--)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    Serial.println(i);
    Serial.println(" speed 3");
    for (int i = 0; i <= 2; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(300);
      digitalWrite(buzzer, LOW);
      delay(200);
    }
    throttle = "0";
  }
  else if (throttle == "four")              //调节滑板速度为4
  {
    int i = ESC.readMicroseconds();
    Serial.print(i);
    Serial.print("--");
    if (i <= speed_max)
      for (i; i <= speed_max; i++)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    else
      for (i; i >= speed_max; i--)
      {
        ESC.writeMicroseconds(i);
        delay(3);
      }
    Serial.println(i);
    Serial.println(" speed 4");
    for (int i = 0; i <= 3; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(300);
      digitalWrite(buzzer, LOW);
      delay(200);
    }
    throttle = "0";
  }
  else if (throttle == "turn on")              //点亮前灯
  {
    digitalWrite(ledPin, HIGH);
    Serial.println(" turn on");
    digitalWrite(buzzer, HIGH);
    delay(500);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  else if (throttle == "turn off")              //熄灭前灯
  {
    digitalWrite(ledPin, LOW);
    Serial.println(" turn off");
    digitalWrite(buzzer, HIGH);
    delay(400);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  else if (throttle == "buzzer on")              //铃铛响
  {
    int times = 2;
    Serial.println(" buzzer on");
    for (int i = 1; i <= times; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(200);
      digitalWrite(buzzer, LOW);
      delay(200);
    }
    throttle = "0";
  }
  else if (throttle == "buzzer off")              //铃铛尖悦响
  {
    int times = 1;
    Serial.println(" buzzer off");
    for (int i = 1; i <= times; i++)
    {
      digitalWrite(buzzer, HIGH);
      delay(100);
      digitalWrite(buzzer, LOW);
      delay(200);
    }
    throttle = "0";
  }
}
