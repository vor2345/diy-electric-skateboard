void ps2(void)
{
  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed
  if (ps2x.ButtonPressed(PSB_START)) //will be TRUE as long as button is pressed
  {
    Serial.println("Start is being held");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
  }
  if (ps2x.ButtonPressed(PSB_SELECT))
  {
    Serial.println("Reset");
    digitalWrite(buzzer, LOW);
    digitalWrite(ledPin, LOW);
    ESC.writeMicroseconds(1000);
  }
  distance();
  if (ps2x.ButtonPressed(PSB_L3))
  {
    digitalWrite(ledPin, HIGH);
    Serial.println(" turn on");
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_R3))
  {
    digitalWrite(ledPin, LOW);
    Serial.println(" turn off");
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_L2))
  {
    throttle = "buzzer on";
    Action(throttle);
  }
  if (ps2x.ButtonPressed(PSB_R2))
  {
    throttle = "buzzer off";
    Action(throttle);
  }
  if (ps2x.ButtonPressed(PSB_L1) || ps2x.ButtonPressed(PSB_R1))
  {
    throttle = "stop";
    Action(throttle);
  }
  if (ps2x.ButtonPressed(PSB_CIRCLE)) //will be TRUE if button was JUST pressed
  {
    int i = ESC.read();
    if (i <= 105)
      for (i; i <= 105; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 105; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1350);
    Serial.println(" speed 1");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_CROSS))//will be TRUE if button was JUST pressed OR released
  {
    int i = ESC.read();
    if (i <= 120)
      for (i; i <= 120; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 120; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1550);
    Serial.println(" speed 2");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_SQUARE))             //will be TRUE if button was JUST released
  {
    int i = ESC.read();
    if (i <= 135)
      for (i; i <= 135; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 135; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1650);
    Serial.println(" speed 3");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
  if (ps2x.ButtonPressed(PSB_TRIANGLE))
  {
    int i = ESC.read();
    if (i <= 150)
      for (i; i <= 150; i = i + 1)
      {
        ESC.write(i);
        delay(15);
      }
    else
      for (i; i >= 150; i = i - 1)
      {
        ESC.write(i);
        delay(15);
      }
    //ESC.writeMicroseconds(1850);
    Serial.println(" speed 4");
    digitalWrite(buzzer, HIGH);
    delay(300);
    digitalWrite(buzzer, LOW);
    throttle = "0";
  }
}
