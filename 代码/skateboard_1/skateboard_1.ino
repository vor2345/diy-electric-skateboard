/*
1.优化相关控制参数，运行更加丝滑
2.由于我买的超声波传感器太便宜了，滑板过坎儿时非常容易容易误触，导致电机熄火，此代码去掉了超声波检测
3.需要用户时刻注意前方道路状况，存在1秒左右的延迟，提前做好判断
  // throttle蓝牙接收指令\n
  speed_min最小控制速度\n
  speed_level启动速度\n
  speed_one速度1\n
  speed_two速度2\n
  speed_three速度3\n
  speed_max速度4，也是最大速度\n
  speed_add单位速度增加量\n
  mTime记录过去的系统时间\n
  test记录当前系统时间\n
  item速度控制实际值\n
  length超声波测的当前距离障碍物距离\n
  heart心跳指示\n
  buzzer定义有源蜂鸣器引脚2\n
  ledPin定义有前灯引脚4\n
  ESC定义无刷电机引脚9\n
  超声波Trig接6，Echo接5\n
  光照检测接A0\n
  无硬件保护

*/


String throttle;
volatile unsigned int speed_min;
volatile unsigned int speed_level;
volatile unsigned int speed_one;
volatile unsigned int speed_two;
volatile unsigned int speed_three;
volatile unsigned int speed_max;
volatile unsigned int speed_add;
volatile long mTime;
volatile long test;
volatile int item;
volatile int length;
volatile int buzzer;
volatile int ledPin;
volatile int ESC;
volatile int light;
volatile boolean heart;

void Action(String throttle) {
  if (throttle == "stop") {
    analogWrite(ESC, speed_level);
    pinMode(buzzer, OUTPUT);
    digitalWrite(buzzer,LOW);
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,LOW);
    item = speed_level;
    buzzer2(1, 500);

  } else if (throttle == "decrease") {
    if (item > speed_level) {
      for (int i = (item); i >= (item - speed_add); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(100);
      }
      item = item - speed_add;
      buzzer2(1, 100);

    }
  } else if (throttle == "increase") {
    if (item < speed_max) {
      for (int i = (item); i <= (item + speed_add); i = i + (1)) {
        analogWrite(ESC, i);
        delay(100);
      }
      item = item + speed_add;
      buzzer2(1, 200);

    }
  } else if (throttle == "one") {
    if (item <= speed_one) {
      for (int i = (item); i <= (speed_one); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_one); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = speed_one;
    buzzer2(1, 150);
  } else if (throttle == "two") {
    if (item <= speed_two) {
      for (int i = (item); i <= (speed_two); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_one); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = speed_two;
    buzzer2(2, 150);
  } else if (throttle == "three") {
    if (item <= speed_three) {
      for (int i = (item); i <= (speed_three); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_three); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = speed_three;
    buzzer2(3, 150);
  } else if (throttle == "four") {
    if (item <= speed_max) {
      for (int i = (item); i <= (speed_max); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_max); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = speed_max;
    buzzer2(4, 150);
  } else if (throttle == "turn on") {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,HIGH);
    buzzer2(1, 200);
  } else if (throttle == "turn off") {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,LOW);
    buzzer2(1, 200);
  } else if (throttle == "buzzer on") {
    buzzer2(2, 300);
  } else if (throttle == "buzzer off") {
    buzzer2(2, 100);
  }
  Serial.print("item=");
  Serial.print(item);
  Serial.println(throttle);
  throttle = "";
}

void lanya() {
  if (Serial.available() > 0) {
    throttle = Serial.readString();
    if (throttle == "start") {
      Serial.println("Bluetooth mode");
      Serial.println("Start,read go!");
      buzzer2(3, 200);
      throttle = "";
      do{
        throttle = Serial.readString();
        if (throttle == "reset") {
          analogWrite(ESC, speed_min);
          pinMode(buzzer, OUTPUT);
          digitalWrite(buzzer,LOW);
          pinMode(ledPin, OUTPUT);
          digitalWrite(ledPin,LOW);
          Serial.println("reset");
          buzzer2(1, 500);
          break;

        } else if (throttle != "reset" && throttle != "") {
          delay(10);
          Action(throttle);
        }
        delay(10);
      }while(true);

    }

  }
}

void buzzer2(int x, int y) {
  for (int i = (1); i <= (x); i = i + (1)) {
    pinMode(buzzer, OUTPUT);
    digitalWrite(buzzer,HIGH);
    delay(y);
    pinMode(buzzer, OUTPUT);
    digitalWrite(buzzer,LOW);
    delay(y);
  }
}

void light_check() {
  light = analogRead(A0);
  if (light < 200) {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,HIGH);

  } else {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,LOW);

  }
}

void setup(){
  throttle = "";
  speed_min = 80;
  speed_level = 100;
  speed_one = 140;
  speed_two = 165;
  speed_three = 190;
  speed_max = 220;
  speed_add = 10;
  mTime = 0;
  test = 0;
  item = speed_level;
  length = 0;
  buzzer = 2;
  ledPin = 4;
  ESC = 9;
  light = 0;
  heart = 0;
  Serial.begin(9600);
  analogWrite(ESC, speed_level);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
  pinMode(buzzer, OUTPUT);
  digitalWrite(buzzer,LOW);
  buzzer2(1, 500);

  pinMode(A0, INPUT);
}

void loop(){
  do{
    lanya();
    delay(10);
    light_check();
  }while(true);

}