
#include <IRremote.h>

long ir_item;
IRrecv irrecv_3(3);
decode_results results_3;

void setup(){
  Serial.begin(9600);
  pinMode(4, OUTPUT);
  irrecv_3.enableIRIn();
}

void loop(){
  if (irrecv_3.decode(&results_3)) {
    ir_item=results_3.value;
    String type="UNKNOWN";
    String typelist[14]={"UNKNOWN", "NEC", "SONY", "RC5", "RC6", "DISH", "SHARP", "PANASONIC", "JVC", "SANYO", "MITSUBISHI", "SAMSUNG", "LG", "WHYNTER"};
    if(results_3.decode_type>=1&&results_3.decode_type<=13){
      type=typelist[results_3.decode_type];
    }
    Serial.print("IR TYPE:"+type+"  ");
    delay(150);
    Serial.print("Receive:  ");
    Serial.println(ir_item);
    if (ir_item == 0xE318261B) {
      digitalWrite(4,HIGH);
      Serial.println("Light turn on");

    }
    if (ir_item == 0xEE886D7F) {
      digitalWrite(4,LOW);
      Serial.println("Light turn off");

    }
    irrecv_3.resume();
  } else {
  }

}
