/*
1.蓝牙遥控滑板主要功能实现
2.由于我买的超声波传感器太便宜了，滑板过坎儿时非常容易容易误触，导致电机熄火
3.需要用户时刻注意前方道路状况，存在1秒左右的延迟，提前做好判断
  // throttle蓝牙接收指令\n
  speed_min最小控制速度\n
  speed_level启动速度\n
  speed_one速度1\n
  speed_two速度2\n
  speed_three速度3\n
  speed_max速度4，也是最大速度\n
  speed_add单位速度增加量\n
  mTime记录过去的系统时间\n
  test记录当前系统时间\n
  item速度控制实际值\n
  length超声波测的当前距离障碍物距离\n
  heart心跳指示\n
  buzzer定义有源蜂鸣器引脚2\n
  ledPin定义有前灯引脚4\n
  ESC定义无刷电机引脚9\n
  超声波Trig接6，Echo接5\n
  光照检测接A0\n
  有超声波传感器硬件保护

*/

String throttle;
volatile unsigned int speed_min;
volatile unsigned int speed_level;
volatile unsigned int speed_one;
volatile unsigned int speed_two;
volatile unsigned int speed_three;
volatile unsigned int speed_max;
volatile unsigned int speed_add;
volatile long mTime;
volatile long test;
volatile int item;
volatile int length;
volatile int buzzer;
volatile int ledPin;
volatile int ESC;
volatile int light;
volatile boolean heart;

void Action(String throttle) {
  Serial.print(item);
  Serial.print("--");
  if (throttle == "stop") {
    analogWrite(ESC, speed_level);
    pinMode(buzzer, OUTPUT);
    digitalWrite(buzzer,LOW);
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,LOW);
    throttle = "";
    item = speed_level;
    buzzer2(1, 100);
    Serial.print("stop");

  } else if (throttle == "decrease") {
    if (item > speed_level) {
      for (int i = (item); i >= (item - speed_add); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = item - speed_add;
    buzzer2(1, 100);
    Serial.print("decrease");
  } else if (throttle == "increase") {
    if (item < speed_max) {
      for (int i = (item); i <= (item + speed_add); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = item + speed_add;
    buzzer2(1, 100);
    Serial.print("increase");
  } else if (throttle == "one") {
    if (item <= speed_one) {
      for (int i = (item); i <= (speed_one); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_one); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = speed_one;
    buzzer2(1, 100);
    Serial.print(" speed 1");
  } else if (throttle == "two") {
    if (item <= speed_two) {
      for (int i = (item); i <= (speed_two); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_one); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    }
    item = speed_two;
    buzzer2(2, 100);
    Serial.print(" speed 2");
  } else if (throttle == "three") {
    if (item <= speed_three) {
      for (int i = (item); i <= (speed_three); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_three); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }
      item = speed_three;
      buzzer2(3, 100);
      Serial.print(" speed 3");

    }
  } else if (throttle == "four") {
    if (item <= speed_max) {
      for (int i = (item); i <= (speed_max); i = i + (1)) {
        analogWrite(ESC, i);
        delay(50);
      }

    } else {
      for (int i = (item); i >= (speed_max); i = i + (-1)) {
        analogWrite(ESC, i);
        delay(50);
      }
      item = speed_max;
      buzzer2(4, 100);
      Serial.print(" speed 4");

    }
  } else if (throttle == "turn on") {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,HIGH);
    buzzer2(1, 100);
    Serial.print("turn on");
  } else if (throttle == "turn off") {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,LOW);
    buzzer2(1, 100);
    Serial.print("turn off");
  } else if (throttle == "buzzer on") {
    buzzer2(2, 200);
    Serial.print("buzzer on");
  } else if (throttle == "buzzer off") {
    buzzer2(2, 100);
    Serial.print("buzzer off");
  }
  Serial.println(item);
  throttle = "";
}

void lanya() {
  if (Serial.available() > 0) {
    throttle = Serial.readString();
    Serial.println("手机蓝牙模式");
    if (throttle == "start") {
      Serial.println("Start,  ready go!");
      buzzer2(3, 100);
      do{
        throttle = "";
        throttle = Serial.readString();
        if (throttle == "reset") {
          analogWrite(ESC, speed_level);
          pinMode(buzzer, OUTPUT);
          digitalWrite(buzzer,LOW);
          pinMode(ledPin, OUTPUT);
          digitalWrite(ledPin,LOW);
          heart = 0;
          break;

        }
        if (throttle == "") {
          heart = 1;
          mTime = millis();

        }
        distance();
        if (throttle != "") {
          Action(throttle);

        }
      }while(true);

    }

  }
}

void buzzer2(int x, int y) {
  for (int i = (1); i <= (x); i = i + (1)) {
    pinMode(buzzer, OUTPUT);
    digitalWrite(buzzer,HIGH);
    delay(y);
    pinMode(buzzer, OUTPUT);
    digitalWrite(buzzer,LOW);
  }
}

float checkdistance_6_5() {
  digitalWrite(6, LOW);
  delayMicroseconds(2);
  digitalWrite(6, HIGH);
  delayMicroseconds(10);
  digitalWrite(6, LOW);
  float distance = pulseIn(5, HIGH) / 58.00;
  delay(10);
  return distance;
}

void distance() {
  length = checkdistance_6_5();
  if (length < 100) {
    length = checkdistance_6_5();
    if (length < 100) {
      throttle = "stop";

    }

  }
}

void light_check() {
  light = analogRead(A0);
  if (light < 200) {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin,HIGH);

  }
}

void setup(){
  throttle = "";
  speed_min = 80;
  speed_level = 100;
  speed_one = 135;
  speed_two = 150;
  speed_three = 175;
  speed_max = 200;
  speed_add = 10;
  mTime = 0;
  test = 0;
  item = 0;
  length = 0;
  buzzer = 2;
  ledPin = 4;
  ESC = 9;
  light = 0;
  heart = 0;
  Serial.begin(9600);
  pinMode(6, OUTPUT);
  pinMode(5, INPUT);
  pinMode(A0, INPUT);
}

void loop(){
  do{
    lanya();
    delay(10);
    light_check();
  }while(true);

}